<?php
function validate_session($p_session_id, $p_user_id)
{
    // Validate the Session
    include 'db_connection.php';
    $conn = new PDO("mysql:host=" . $db_servername . ";dbname=" . $db_name, $db_username, $db_password);
    
    $sql = $conn->prepare("SELECT id, user_id, creation_time, last_active
                             FROM session
                            WHERE id = :session_id AND user_id = :user_id
                              AND last_active + interval get_int_parameter('SESSION', 1) minute >= CURRENT_TIMESTAMP");
    
    $sql->execute(array(':session_id' => $p_session_id, ':user_id' => $p_user_id));

    if ($sql->rowCount() == 1){
        // update session.last_active and return true
        $sql = $conn->prepare("UPDATE session SET last_active = CURRENT_TIMESTAMP WHERE id = :session_id");
        $sql->execute(array(':session_id' => $p_session_id));
        return true;
    }
    else{
        return false;
    }
}
?>