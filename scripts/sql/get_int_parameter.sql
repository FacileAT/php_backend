DELIMITER $$
 
CREATE FUNCTION get_int_parameter(p_param_name VARCHAR(30), p_element INT) RETURNS INT
    NOT DETERMINISTIC
BEGIN
    DECLARE param_val_1 INT;
    DECLARE param_val_2 INT;

    SELECT int_value1, int_value2
      INTO param_val_1, param_val_2
      FROM parameter
     WHERE param_name = p_param_name;
 
    IF p_element = 1 THEN
        RETURN (param_val_1);
    else
        RETURN (param_val_2);
    END IF;
END
