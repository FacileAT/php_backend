CREATE TABLE routes(
    id INT(6)   UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    route_method    VARCHAR(10) NOT NULL DEFAULT "GET",
    route_address   varchar(100) NOT NULL,
    script_address  VARCHAR(50) NOT NULL,
    required_roles  VARCHAR(50),
    require_login   BOOLEAN NOT NULL DEFAULT 0,
    active          BOOLEAN NOT NULL DEFAULT 0,
    UNIQUE KEY (route_method, route_address)
)