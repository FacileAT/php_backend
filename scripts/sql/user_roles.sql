CREATE TABLE user_roles (
    user_id     INT(6),
    role_id     INT(6),
    date_begin  DATE,
    date_end    DATE,
    PRIMARY KEY(user_id, role_id, date_begin)
)