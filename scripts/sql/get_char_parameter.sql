DELIMITER $$
 
CREATE FUNCTION get_char_parameter(p_param_name VARCHAR(30), p_element INT) RETURNS VARCHAR(100)
    NOT DETERMINISTIC
BEGIN
    DECLARE param_val_1 varchar(100);
    DECLARE param_val_2 varchar(100);

    SELECT char_value1, char_value2
      INTO param_val_1, param_val_2
      FROM parameter
     WHERE param_name = p_param_name;
 
    IF p_element = 1 THEN
        RETURN (param_val_1);
    else
        RETURN (param_val_2);
    END IF;
END
