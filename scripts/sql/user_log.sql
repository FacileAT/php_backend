CREATE TABLE user_log (
    id INT(8)   UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    log_date    TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    user_id     INT(6),
    event       VARCHAR(50)
)