CREATE TABLE session (
    id              VARCHAR(64) PRIMARY KEY,
    user_id         INT(6),
    creation_time   TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    last_active     TIMESTAMP NOT NULL
)