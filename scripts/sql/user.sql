CREATE TABLE user (
    id INT(6)   UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    first_name  VARCHAR(30) NOT NULL,
    last_name   VARCHAR(30) NOT NULL,
    login_name  VARCHAR(30) UNIQUE NOT NULL,
    password    VARCHAR(64) NOT NULL,
    status      INT(1) NOT NULL DEFAULT 0
)