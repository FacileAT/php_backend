CREATE TABLE parameter (
    param_name  VARCHAR(30) PRIMARY KEY,
    char_value1 VARCHAR(100),
    char_value2 VARCHAR(100),
    int_value1  INT,
    int_value2  INT
)