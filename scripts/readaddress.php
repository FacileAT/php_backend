<html>

<head>
  <title>PHP Backend Testscript</title>
</head>

<body>
<?php
  echo "<h1>Testscript for reading the Server variables and given input</h1>";
  
  echo "<h2>PHP Info ()</h2>";
  phpinfo();
  
  echo "<h2>Read the Server variables</h2>";
  while (list($var,$value) = each ($_SERVER)) {
    echo "$var => $value <br />";
  }

  echo "<h2>Extract the required information</h2>";
  echo "<table><thead><th><td>Variable</td><td>Value</td></th></thead><tbody>";
  echo "<tr><td>Route</td><td>" . $_SERVER['QUERY_STRING'] . "</td></tr>";
  echo "</tbody></table>";

  echo "<h2>Detect the right route</h2>";
  $route1 = "/login";
  $route2 = "/user/:userId/userinfo";
  $tmpRequestedRoute = explode("/", $_SERVER['QUERY_STRING']);
  $tmpAvaiableRoutes = explode("/", $route2);
  if(count($tmpAvaiableRoutes) == count($tmpRequestedRoute)){
    $route_found = 1;
    
    for ($i = 0; $i < count($tmpRequestedRoute); $i++){
      if ($tmpRequestedRoute[$i] !== $tmpAvaiableRoutes[$i] && $tmpAvaiableRoutes[$i][0] != ":"){
        $route_found = 0;
      }
    }

    if ($route_found == 1){
      echo $tmpAvaiableRoutes[1] . "<br>";
      echo count($tmpAvaiableRoutes);
    }
    else {
      echo "Route Elements matched, but no route found<br>";
    }
  }
  else {
    echo "The requested route does not match any route length";
  }

?>
</body>
</html>