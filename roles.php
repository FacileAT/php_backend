<?php
function check_roles($p_route_id, $p_user_id)
{
    // Check, if the user has the required role for the requested route
    include 'db_connection.php';
    $conn = new PDO("mysql:host=" . $db_servername . ";dbname=" . $db_name, $db_username, $db_password);
    
    $sql = $conn->prepare("SELECT 'X' FROM dual
                            WHERE EXISTS (SELECT 'X' FROM routes
                                           WHERE routes.id = :route_id
                                             AND IFNULL(required_roles, '0') = '0')
                               OR EXISTS (SELECT 'X' FROM user_roles
                                           WHERE user_id = :user_id
                                             AND CURRENT_DATE BETWEEN date_begin AND date_end
                                             AND role_id in (SELECT required_roles FROM routes
                                                              WHERE routes.id = :route_id))");
    
    $sql->execute(array(':route_id' => $p_route_id, ':user_id' => $p_user_id));

    if ($sql->rowCount() > 0){
        // User has the required role or the route has no restrictions to any role (required_roles = 0)
        return true;
    }
    else{
        return false;
    }
}
?>