<?php
/** +=============================================================+
 *  | Find the right route in database and run appropriate script |
 *  +=============================================================+
 * 
 *  The connection settings for the database are defined in file
 *  db_connection.php
 * 
 * 
 */

$route_id = 0;
$route_script  = "";
$cookie_name = "FS_session_id";
$sys_session = "";

require 'db_connection.php';
require 'session.php';
require 'roles.php';

// parse requested route to an array
$tmpRequestedRoute = explode("/", urldecode($_SERVER['QUERY_STRING']));

// Create DB connection with PDO
$conn = new PDO("mysql:host=" . $db_servername . ";dbname=" . $db_name, $db_username, $db_password);

// Query all available routes
$sql = $conn->prepare("SELECT id, route_method, route_address, script_address, required_roles, require_login, active
                         FROM routes
                        WHERE route_method = :request_method");

$sql->execute(array(':request_method' => $_SERVER['REQUEST_METHOD']));

if ($sql->rowCount() > 0){
    while ($row = $sql->fetch()){
        // Has the actual route the same number of elements?
        $tmpActualRoute = explode("/", $row['route_address']);
        if (count($tmpActualRoute) == count($tmpRequestedRoute)){
            // Check, if the actual route matches the requested route
            $tmpRouteFound = 1;

            for ($i = 0; $i < count($tmpRequestedRoute); $i++){
                if ($tmpRequestedRoute[$i] !== $tmpActualRoute[$i] && $tmpActualRoute[$i][0] != ":"){
                    $tmpRouteFound = 0;
                }
            }

            if ($tmpRouteFound == 1){
                // Is the requested route active?
                if ($row['active'] == 1){
                    if ($row['require_login'] == 1){
                        // Authentification is required
                        // Check, if the user is logged in and the session is valid
                        if(!isset($_COOKIE[$cookie_name])) {
                            $sys_session  = "";
                            $route_id     = -1;
                            $route_script = "";
                            echo "Please log on to access this route!";
                            break;
                        }
                        else {
                            // Cookie found => Check if the session is valid
                            $sys_session = explode("##", $_COOKIE[$cookie_name]);
                            if (validate_session($sys_session[1], $sys_session[0]) === false){
                                $sys_session  = "";
                                $route_id     = -1;
                                $route_script = "";
                                setcookie($cookie_name, "", time() - 3600);
                                echo "Your session has expired! Please log on again.";
                                break;
                            }

                            // Check, if the user has the required role
                            if (check_roles($row['id'], $sys_session[0]) === false){
                                $sys_session  = "";
                                $route_id     = -1;
                                $route_script = "";
                                setcookie($cookie_name, "", time() - 3600);
                                echo "You don't have the required role to access this route!";
                                break;
                            }
                        }
                    }
                    $route_id = $row['id'];
                    $route_script = $row['script_address'];
                }
                else {
                    $route_id = -1;
                    echo "The requested route is not active!";
                }
                break;
            }
        }
    }

    // If $route_id == 0: the requested route doesn't exist in database
    // otherwise run the appropriate script
    if ($route_id > 0){
        require 'execute/' . $route_script;
    }
    else {
        if ($route_id == 0){
            echo "The requested route doesn't exist in database!";
        }
    }
}
else {
    echo "No routes defined in database!";
}

// Close DB connection
$sql = null;
$conn = null;
?>