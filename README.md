# PHP Backend System

PHP based REST API


## 1. Prepare Database and Config Files


## 2. Database
All SQL Statements for creating the Database objects are in the folder scripts/sql.

### Parameter
|Column     |Datatype    |Primary Key|Description                      |
|-----------|------------|:---------:|---------------------------------|
|param_name |VARCHAR(30) |X          |Unique parameter name            |
|char_value1|VARCHAR(100)|           |Custom charcter value            |
|char_value2|VARCHAR(100)|           |Custom charcter value            |
|int_value1 |INT         |           |Custom integer value             |
|int_value2 |INT         |           |Custom integer value             |

### Roles
|Column    |Datatype    |Primary Key|Description                      |
|----------|------------|:---------:|---------------------------------|
|id        |INT(6)      |X          |                                 |
|role_name |VARCHAR(30) |           |The role_name must be unique!    |
|active    |BOOLEAN     |           |0...Role inactive<br>1...Role active |

### Routes
|Column        |Datatype    |Primary Key|Description                      |
|--------------|------------|:---------:|---------------------------------|
|id            |INT(6)      |X          |                                 |
|route_method  |VARCHAR(10) |UNIQUE KEY |GET, POST, INSERT, DELETE, OPTION|
|route_address |VARCHAR(100)|UNIQUE KEY |Route of the API address<br>dynamic elements are indicated by curly brackets|
|script_address|VARCHAR(50) |           |Path of PHP script               |
|required_roles|VARCHAR(50) |           |List of RoleIDs, which are allowed to access this route|
|require_login |BOOLEAN     |           |0...No login required<br>1...Login required|
|active        |BOOLEAN     |           |0...Route inactive<br>1...Route active |

### Session
|Column       |Datatype    |Primary Key|Description                      |
|-------------|------------|:---------:|---------------------------------|
|id           |VARCHAR(64) |X          |SHA256 SessionID                 |
|user_id      |INT(6)      |           |UserID from table user           |
|creation_time|TIMESTAMP   |           |Creation timestamp of the session|
|last_active  |TIMESTAMP   |           |Timestamp of the last user action|

### User
|Column    |Datatype    |Primary Key|Description                      |
|----------|------------|:---------:|---------------------------------|
|id        |INT(6)      |X          |                                 |
|first_name|VARCHAR(30) |           |                                 |
|last_name |VARCHAR(30) |           |                                 |
|login_name|VARCHAR(30) |           |The login_name must be unique!   |
|password  |VARCHAR(64) |           |SHA256 Password String           |
|status    |INT(1)      |           |0...User inactive<br>1...User active<br>2...User active, but password must be changed|

### User_Log
|Column    |Datatype    |Primary Key|Description                      |
|----------|------------|:---------:|---------------------------------|
|id        |INT(8)      |X          |                                 |
|log_date  |TIMESTAMP   |           |Timestamp of the event           |
|user_id   |INT(6)      |           |UserID from table user           |
|event     |VARCHAR(50) |           |Freely selectable description of the log|

### User_Roles
|Column    |Datatype    |Primary Key|Description                      |
|----------|------------|:---------:|---------------------------------|
|user_id   |INT(8)      |X          |UserID from table user           |
|role_id   |INT(6)      |X          |RoleID from table roles          |
|date_begin|DATE        |X          |date from which the role is active|
|date_end  |DATE        |           |date until which the role is active|


## 3. Parameter
Each parameter has 2 character and 2 integer values. For easier reading, the stored functions get_char_parameter and get_int_parameter can be used directly inside an SELECT statement.

|Parameter Name |Char value 1|Char Value 2|Integer value 1|Integer value 2|
|---------------|------------|------------|---------------|---------------|
|SESSION        |            |            |Inactive time in minutes, from which a session should be set as invalid|               |
