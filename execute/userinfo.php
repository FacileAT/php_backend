<?php
/** +=============================================================+
 *  | Return the userinfo as JSON array                           |
 *  +=============================================================+
 * 
 *  The RouteID is located in variable $route_id
 * 
 *  Return value:
 *  {id, first_name, last_name}
 */

// TODO: only return user values if the logged in userid eqals the requested user id (maybe except of a special role?)
$userID = $tmpRequestedRoute[2];
echo "USERINFO ID=" . $route_id . " | UserID=" . $userID;
?>