<?php
/** +=============================================================+
 *  | Login to the server                                         |
 *  +=============================================================+
 * 
 *  The RouteID is located in variable $route_id
 * 
 *  Return value:
 *  {id, full_name, session_id}
 */

$sql = $conn->prepare("SELECT id, first_name, last_name, status
                         FROM user
                        WHERE login_name = :login_name
                          AND password = :password");
                       
$sql->execute(array(':login_name' => $_POST['login_name'], ':password' => hash("SHA256", $_POST['password'])));

if ($sql->rowCount() == 1){
    while ($row = $sql->fetch()){
        if ($row['status'] == 1){
            //Create new Session and write it to database
            $session_id = hash("SHA256", time() . $_SERVER['REMOTE_ADDR'] . $row['id'] . $row['first_name'] . $row['last_name']);

            $sql = $conn->prepare("INSERT INTO session (id, user_id, creation_time, last_active)
                                   VALUES (:session_id, :user_id, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)");
        
            $sql->execute(array(':session_id' => $session_id, ':user_id' => $row['id']));
            echo "{userId:" . $row['id'] . ",userName:" . $row['first_name'] . " " . $row['last_name'] . ",sessionId:" . $session_id . "}";

            // Set cookie
            setcookie($cookie_name, $row['id'] . "##" . $session_id);
        }
        else {
            // User is not active
            echo "Useraccount is not active!";
        }
    }

    // Delete old sessions in the database
    $sql = $conn->prepare("DELETE FROM session WHERE last_active + interval get_int_parameter('SESSION', 1) minute < CURRENT_TIMESTAMP");
    $sql->execute();
}
else {
    echo "Wrong Username or Password!";
}
?>